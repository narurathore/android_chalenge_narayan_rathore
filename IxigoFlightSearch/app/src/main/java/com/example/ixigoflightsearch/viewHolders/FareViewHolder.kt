package com.example.ixigoflightsearch.viewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ixigoflightsearch.R
import com.example.ixigoflightsearch.models.AppendixOfFlightsModel
import com.example.ixigoflightsearch.models.FareDetailsModel
import com.example.ixigoflightsearch.models.FlightDetailModel

class FareViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) {

    /**
     * Binds the fare data to view
     * @param fareDetail
     * @param appendixOfFlights
     */
    fun onBind(fareDetail: FareDetailsModel, appendixOfFlights: AppendixOfFlightsModel){

        val providerName = itemView.findViewById<TextView>(R.id.provider_name)
        val price = itemView.findViewById<TextView>(R.id.price)

        providerName.text = appendixOfFlights.getProvideName(fareDetail.providerId)
        price.text = "Rs " + fareDetail.fare

    }
}