package com.example.ixigoflightsearch.repositories

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.ixigoflightsearch.models.BaseDataModel
import com.example.ixigoflightsearch.models.SearchFlightResultsModel

class RemoteRepository(val context: Context) : RepositoryInterface {

    /**
     * Will returns the search flight data from server
     * @param onSuccess
     * @param onFailure
     */
    override fun getSearchData(onSuccess: Response.Listener<BaseDataModel>, onFailure: Response.ErrorListener) {
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(context)
        val url = "http://www.mocky.io/v2/5979c6731100001e039edcb3"

        // Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                try {
                    var searchFlightResultsModel = SearchFlightResultsModel()
                    onSuccess.onResponse(searchFlightResultsModel.parseResponse(response))
                } catch (ex : Exception){
                    onFailure.onErrorResponse(VolleyError("Something went wrong"))
                }
            },
            Response.ErrorListener {
                onFailure.onErrorResponse(it)
            })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }
}