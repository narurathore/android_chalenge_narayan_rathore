package com.example.ixigoflightsearch.models

import com.google.gson.annotations.SerializedName

data class FareDetailsModel(
    @SerializedName("providerId")
    val providerId : Int? = null,

    @SerializedName("fare")
    val fare : Int? = null

): BaseDataModel()