package com.example.ixigoflightsearch.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.Response
import com.android.volley.VolleyError
import com.example.ixigoflightsearch.models.BaseDataModel
import com.example.ixigoflightsearch.models.SearchFlightResultsModel
import com.example.ixigoflightsearch.repositories.RepositoryInterface
import java.util.*


class SearchFlightViewModel (var repository: RepositoryInterface?): ViewModel(), Response.ErrorListener,
    Response.Listener<BaseDataModel> {

    val instruct = MutableLiveData<Instruct>()
    val searchFlightsData = MutableLiveData<SearchFlightResultsModel>()

    override fun onResponse(response: BaseDataModel?) {
        instruct.value = Instruct.showOrHideProgressBar(false)
        if (response is SearchFlightResultsModel){
            searchFlightsData.value = response
        }
    }

    override fun onErrorResponse(error: VolleyError?) {
        instruct.value = Instruct.showOrHideProgressBar(false)
        error?.let {
            instruct.value = Instruct.showErrorDialog(it)
        }
    }

    init {
        getSearchData()
    }

    /**
     * fetch data from server
     */
    fun getSearchData(){
        instruct.value = Instruct.showOrHideProgressBar(true)
        repository?.getSearchData(this, this)
    }

    /**
     * Sort flight lists according to cheapest first
     */
    fun sortAccordingToCheapestFirst(){
        if (searchFlightsData.value != null){
            Collections.sort(searchFlightsData.value?.flightsList) { p0, p1 ->
                p0!!.getCheapestfare() - p1!!.getCheapestfare()
            }
            searchFlightsData.postValue(searchFlightsData.value)
        }
    }

    /**
     * Sort flight lists according to fastest first
     */
    fun sortAccordingToFastestFirst(){
        if (searchFlightsData.value != null){
            Collections.sort(searchFlightsData.value?.flightsList) { p0, p1 ->
                (p0!!.getDurationInMiliSecs() - p1!!.getDurationInMiliSecs()).toInt()
            }
            searchFlightsData.postValue(searchFlightsData.value)
        }
    }

    /**
     * Sort flight lists according to early departure
     */
    fun sortAccordingToEarlyDeparture(){
        if (searchFlightsData.value != null){
            Collections.sort(searchFlightsData.value?.flightsList) { p0, p1 ->
                (p0!!.departureTime!! - p1!!.departureTime!!).toInt()
            }
            searchFlightsData.postValue(searchFlightsData.value)
        }
    }

    /**
     * Sort flight lists according to late departure
     */
    fun sortAccordingToLateDeparture(){
        if (searchFlightsData.value != null){
            Collections.sort(searchFlightsData.value?.flightsList) { p0, p1 ->
                (p1!!.departureTime!! - p0!!.departureTime!!).toInt()
            }
            searchFlightsData.postValue(searchFlightsData.value)
        }
    }

    /**
     * Sort flight lists according to early arrival
     */
    fun sortAccordingToEarlyArrival(){
        if (searchFlightsData.value != null){
            Collections.sort(searchFlightsData.value?.flightsList) { p0, p1 ->
                (p0!!.arrivalTime!! - p1!!.arrivalTime!!).toInt()
            }
            searchFlightsData.postValue(searchFlightsData.value)
        }
    }

    /**
     * Sort flight lists according to late arrival
     */
    fun sortAccordingToLateArrival(){
        if (searchFlightsData.value != null){
            Collections.sort(searchFlightsData.value?.flightsList) { p0, p1 ->
                (p1!!.arrivalTime!! - p0!!.arrivalTime!!).toInt()
            }
            searchFlightsData.postValue(searchFlightsData.value)
        }
    }
}
