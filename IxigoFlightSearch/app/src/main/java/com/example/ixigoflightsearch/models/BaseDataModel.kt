package com.example.ixigoflightsearch.models

import com.google.gson.Gson
import java.io.Serializable

abstract class BaseDataModel : Serializable {

    fun parseResponse(response: String) : BaseDataModel {
        return Gson().fromJson(response, this.javaClass) as BaseDataModel
    }
}