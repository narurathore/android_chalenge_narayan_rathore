package com.example.ixigoflightsearch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ixigoflightsearch.R
import com.example.ixigoflightsearch.models.AppendixOfFlightsModel
import com.example.ixigoflightsearch.models.FareDetailsModel
import com.example.ixigoflightsearch.models.SearchFlightResultsModel
import com.example.ixigoflightsearch.viewHolders.FareViewHolder
import com.example.ixigoflightsearch.viewHolders.FlightDetailsViewHolder

class FareAdapter (val faresList: List<FareDetailsModel>, val appendixOfFlights: AppendixOfFlightsModel):
                        RecyclerView.Adapter<FareViewHolder>() {

    override fun getItemCount(): Int {
        return faresList.size
    }

    override fun onBindViewHolder(holder: FareViewHolder, position: Int) {
        holder.onBind(faresList[position], appendixOfFlights)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FareViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem = layoutInflater.inflate(R.layout.fare_recycler_item, parent, false)
        return FareViewHolder(listItem)
    }
}