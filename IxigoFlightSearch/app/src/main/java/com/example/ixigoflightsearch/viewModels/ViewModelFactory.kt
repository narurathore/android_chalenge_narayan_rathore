package com.example.ixigoflightsearch.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.ixigoflightsearch.repositories.RepositoryInterface
import kotlin.reflect.KFunction1

@Suppress("UNCHECKED_CAST")
class ViewModelFactory (private val builder: Builder): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchFlightViewModel(builder.repository) as T
    }

    class Builder {

        var repository: RepositoryInterface? = null
            private set

        fun setRepository(repository: RepositoryInterface): Builder {
            this.repository = repository
            return this
        }
    }

}