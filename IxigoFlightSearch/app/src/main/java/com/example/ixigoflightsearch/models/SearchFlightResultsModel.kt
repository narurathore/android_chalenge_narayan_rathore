package com.example.ixigoflightsearch.models

import com.google.gson.annotations.SerializedName

data class SearchFlightResultsModel(

    @SerializedName("appendix")
    val appendix : AppendixOfFlightsModel? = null,

    @SerializedName("flights")
    val flightsList : List<FlightDetailModel>? = null

): BaseDataModel()