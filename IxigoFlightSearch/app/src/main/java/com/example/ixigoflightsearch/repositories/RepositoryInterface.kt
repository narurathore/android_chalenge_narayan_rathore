package com.example.ixigoflightsearch.repositories

import com.android.volley.Response
import com.example.ixigoflightsearch.models.BaseDataModel

interface RepositoryInterface {

    fun getSearchData(onSuccess: Response.Listener<BaseDataModel>, onFailure: Response.ErrorListener)
}