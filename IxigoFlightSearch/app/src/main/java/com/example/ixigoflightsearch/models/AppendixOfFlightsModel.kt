package com.example.ixigoflightsearch.models

import com.google.gson.annotations.SerializedName

data class AppendixOfFlightsModel (

    @SerializedName("airlines")
    val airlines : HashMap<String, String>? = null,

    @SerializedName("airports")
    val airports : HashMap<String, String>? = null,

    @SerializedName("providers")
    val providers : HashMap<String, String>? = null
) : BaseDataModel() {

    /**
     * Returns airline Name according to airline code
     * @return String?
     * @param airlineCode
     */
    fun getAirlineName(airlineCode: String?): String?{
        if (airlines?.containsKey(airlineCode) == true){
            return airlines[airlineCode]
        }
        return null
    }

    /**
     * Returns airport Name according to airport code
     * @return String?
     * @param code
     */
    fun getAirportName(code: String?): String?{
        if (airports?.containsKey(code) == true){
            return airports[code]
        }
        return null
    }

    /**
     * Returns provide Name according to provider id
     * @return String?
     * @param id
     */
    fun getProvideName(id: Int?): String?{
        if (providers?.containsKey(id.toString()) == true){
            return providers[id.toString()]
        }
        return null
    }
}