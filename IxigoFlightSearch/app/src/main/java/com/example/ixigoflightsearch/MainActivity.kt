package com.example.ixigoflightsearch

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.ixigoflightsearch.adapters.SearchFlightsAdapter
import com.example.ixigoflightsearch.models.SearchFlightResultsModel
import com.example.ixigoflightsearch.repositories.RemoteRepository
import com.example.ixigoflightsearch.viewModels.Instruct
import com.example.ixigoflightsearch.viewModels.SearchFlightViewModel
import com.example.ixigoflightsearch.viewModels.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog


class MainActivity : AppCompatActivity(), LifecycleOwner {

    var viewModel: SearchFlightViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = createViewModel()
        viewModel?.searchFlightsData?.observe(this, Observer { setSearchDataRecyclerView(it) })
        viewModel?.instruct?.observe(this, Observer { performInstruction(it) })
    }

    /**
     * @return new View model
     */
    private fun createViewModel(): SearchFlightViewModel? {
        return ViewModelProviders.of(this, ViewModelFactory(ViewModelFactory.Builder()
            .setRepository(RemoteRepository(this))))[SearchFlightViewModel::class.java]
    }

    /**
     * set flight search data in recycler view
     * @param searchFlightData
     */
    private fun setSearchDataRecyclerView(searchFlightData: SearchFlightResultsModel){
        search_data_recycler_view.adapter = SearchFlightsAdapter(searchFlightData, this)
    }

    /**
     * performs instructions provided
     * @param instruct
     */
    private fun performInstruction(instruct: Instruct){
        when (instruct){
            is Instruct.showOrHideProgressBar -> {
                if (instruct.show){
                    progress_bar.visibility = View.VISIBLE
                } else {
                    progress_bar.visibility = View.GONE
                }
            }
            is Instruct.showErrorDialog -> {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(instruct.volleyError.message)
                    .setPositiveButton(R.string.retry_button_text,
                        DialogInterface.OnClickListener { dialog, id ->
                            viewModel?.getSearchData()
                            dialog.dismiss()
                        })
                builder.show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.cheapest -> viewModel?.sortAccordingToCheapestFirst()
            R.id.fastest -> viewModel?.sortAccordingToFastestFirst()
            R.id.early_departure -> viewModel?.sortAccordingToEarlyDeparture()
            R.id.late_departure -> viewModel?.sortAccordingToLateDeparture()
            R.id.early_arrival -> viewModel?.sortAccordingToEarlyArrival()
            R.id.late_arrival -> viewModel?.sortAccordingToLateArrival()
        }
        return super.onOptionsItemSelected(item)
    }
}
