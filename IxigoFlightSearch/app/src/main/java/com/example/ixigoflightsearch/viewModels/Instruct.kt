package com.example.ixigoflightsearch.viewModels

import com.android.volley.VolleyError

sealed class Instruct {
    class showOrHideProgressBar(val show: Boolean) : Instruct()

    class showErrorDialog(val volleyError: VolleyError) : Instruct()
}