package com.example.ixigoflightsearch.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ixigoflightsearch.models.SearchFlightResultsModel
import com.example.ixigoflightsearch.viewHolders.FlightDetailsViewHolder
import android.view.LayoutInflater
import com.example.ixigoflightsearch.R


class SearchFlightsAdapter(val flightsData: SearchFlightResultsModel, val context: Context): RecyclerView.Adapter<FlightDetailsViewHolder>() {
    override fun getItemCount(): Int {
        return flightsData.flightsList?.size?:0
    }

    override fun onBindViewHolder(holder: FlightDetailsViewHolder, position: Int) {
        holder.onBind(flightsData.flightsList!![position], flightsData.appendix!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightDetailsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem = layoutInflater.inflate(R.layout.flight_detail_recycler_item, parent, false)
        return FlightDetailsViewHolder(listItem, context)
    }
}