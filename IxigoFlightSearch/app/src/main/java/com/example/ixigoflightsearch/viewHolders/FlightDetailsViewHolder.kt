package com.example.ixigoflightsearch.viewHolders

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ixigoflightsearch.R
import com.example.ixigoflightsearch.adapters.FareAdapter
import com.example.ixigoflightsearch.models.AppendixOfFlightsModel
import com.example.ixigoflightsearch.models.FlightDetailModel
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration


class FlightDetailsViewHolder(itemView : View, val context: Context) : RecyclerView.ViewHolder(itemView) {

    /**
     * /**
     * Binds the flight data to view
     * @param flightDetail
     * @param appendixOfFlights
    */
     */
    fun onBind(flightDetail: FlightDetailModel, appendixOfFlights: AppendixOfFlightsModel){

        val flightName = itemView.findViewById<TextView>(R.id.flight_name)
        val duration = itemView.findViewById<TextView>(R.id.duration)
        val departureArriavalTime = itemView.findViewById<TextView>(R.id.departure_arrival_time)
        val classType = itemView.findViewById<TextView>(R.id.class_type)
        val fromTo = itemView.findViewById<TextView>(R.id.from_to)
        val fareRecyclerView = itemView.findViewById<RecyclerView>(R.id.fare_recycler_view)

        flightName.text = appendixOfFlights.getAirlineName(flightDetail.airlineCode)
        classType.text = flightDetail.classType
        duration.text = flightDetail.getDuration()
        departureArriavalTime.text = flightDetail.getDisplayDepartureArrivalTime()
        fromTo.text = flightDetail.getDisplayFromToAirports(appendixOfFlights)
        fareRecyclerView.adapter = FareAdapter(flightDetail.fareDetailsList!!, appendixOfFlights)
        fareRecyclerView.addItemDecoration(getFareItemDecorator())
    }

    /**
     * @return the decorator for recycler view
     */
    private fun getFareItemDecorator(): DividerItemDecoration{
        val itemDecorator =  DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(context, R.drawable.fare_recycler_decorator)!!)
        return itemDecorator
    }
}