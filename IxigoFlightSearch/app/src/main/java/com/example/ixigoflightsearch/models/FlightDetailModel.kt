package com.example.ixigoflightsearch.models

import com.google.gson.annotations.SerializedName
import java.util.*
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.Instant
import java.time.Instant.MAX
import kotlin.math.min


data class FlightDetailModel(

    @SerializedName("originCode")
    val originCode : String? = null,

    @SerializedName("destinationCode")
    val destinationCode : String? = null,

    @SerializedName("departureTime")
    val departureTime : Long? = null,

    @SerializedName("arrivalTime")
    val arrivalTime : Long? = null,

    @SerializedName("airlineCode")
    val airlineCode : String? = null,

    @SerializedName("class")
    val classType : String? = null,

    @SerializedName("fares")
    val fareDetailsList: List<FareDetailsModel>? = null

) : BaseDataModel() {

    /**
     * @return Display String for departure and arrival time
     */
    fun getDisplayDepartureArrivalTime(): String?{
        if (arrivalTime != null && departureTime != null) {
            val sdf = SimpleDateFormat("HH:mm")
            return sdf.format(getTime(departureTime)) + " -> " + sdf.format(getTime(arrivalTime))
        }
        return null
    }

    /**
     * @return Date from milliseconds
     * @param milliseconds
     */
    fun getTime(milliseconds: Long): Date{
        val cal = Calendar.getInstance()
        cal.timeInMillis = milliseconds
        return cal.time
    }

    /**
     * @return Duration display string of flight
     */
    fun getDuration(): String?{
        if (arrivalTime != null && departureTime != null){
            val durationInMiliSecs = arrivalTime - departureTime
            val hours = durationInMiliSecs / (60 * 60 * 1000) % 24
            val mins = durationInMiliSecs / (60 * 1000) % 60
            return hours.toInt().toString() + "h " + mins.toInt().toString() + "m"
        }
        return null
    }

    /**
     * @return From and To display string of flight
     * @param appendixOfFlights
     */
    fun getDisplayFromToAirports(appendixOfFlights: AppendixOfFlightsModel): String?{
        return appendixOfFlights.getAirportName(originCode) + " -> " + appendixOfFlights.getAirportName(destinationCode)
    }

    /**
     * @return The cheapest fare between the providers
     */
    fun getCheapestfare(): Int{
        var min:Int = Integer.MAX_VALUE
        fareDetailsList?.forEach {
            if (it.fare!! < min){
                min = it.fare
            }
        }
        return min
    }

    /**
     * @return return the duration of flights in milliseconds
     */
    fun getDurationInMiliSecs(): Long{
        if (arrivalTime != null && departureTime != null){
            return arrivalTime - departureTime
        }
        return 0
    }
}